package ssm.view;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TOOLTIP_ADD_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_EDIT_SLIDE_SHOW_TITLE;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT;
import static ssm.LanguagePropertyType.TOOLTIP_LOAD_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_DOWN;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_UP;
import static ssm.LanguagePropertyType.TOOLTIP_NEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_SAVE_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_EDIT_SLIDE_SHOW_TITLE;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_MOVE_DOWN;
import static ssm.StartupConstants.ICON_MOVE_UP;
import static ssm.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_WINDOW;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.controller.FileController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing slide shows.
 * 
 * @author McKilla Gorilla & _____________
 */
public class SlideShowMakerView {
    //THIS VALUE HOLD THE SELECTED SLIDE
    Slide selectedSlide;
    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;
    
    int viewIndex = 0;
    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button editSlideShowTitleButton;
    Button exitButton;
    Label titleLabel;
    
    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveUpButton;
    Button moveDownButton;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);
        selectedSlide = slideShow.getSelectedSlide();

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
	return slideShow;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox();
	slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	addSlideButton = this.initChildButton(slideEditToolbar,	ICON_ADD_SLIDE,TOOLTIP_ADD_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	removeSlideButton = this.initChildButton(slideEditToolbar, ICON_REMOVE_SLIDE, TOOLTIP_REMOVE_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	moveUpButton = this.initChildButton(slideEditToolbar, ICON_MOVE_UP, TOOLTIP_MOVE_UP, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        moveDownButton = this.initChildButton(slideEditToolbar, ICON_MOVE_DOWN, TOOLTIP_MOVE_DOWN, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        
        
        // AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
        
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(slidesEditorScrollPane);
    }
    
    //ENTER TITLE GUI AND EVENT HANDLERS
    public void initEnterTitle(){
        // CREATE GUI COMPONENTS, LANGUAGE PROPERTY, STRING TO BE RETURNED
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        LanguagePropertyType lpt = TOOLTIP_EDIT_SLIDE_SHOW_TITLE; 
        Stage stage = new Stage();
        stage.setMinHeight(100);
        stage.setMinWidth(100);
        VBox pane = new VBox();
        Button ok = new Button("OK");
        TextField txt = new TextField();
        Label label = new Label(props.getProperty(lpt.toString()));
        label.setStyle("-fx-font-weight: bold");
        stage.getIcons().add(new Image("file:" + PATH_ICONS + ICON_WINDOW));
        pane.getChildren().addAll(
                label,
                txt, 
                ok
        );
        //ADD EVENT LISTENER
        ok.setOnAction(e ->{
           slideShow.setTitle(txt.getText());
           stage.close();
           
        });
        stage.setScene(new Scene(pane));
        stage.showAndWait();
        
    }
    
    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
	newSlideShowButton.setOnAction(e -> {
	    fileController.handleNewSlideShowRequest();
	});
	loadSlideShowButton.setOnAction(e -> {
	    fileController.handleLoadSlideShowRequest();
	});
	saveSlideShowButton.setOnAction(e -> {
	    fileController.handleSaveSlideShowRequest();
	});
        viewSlideShowButton.setOnAction(e ->{
            viewSlideShow();
        });
        editSlideShowTitleButton.setOnAction(e -> {
            fileController.handleEditSlideShowTitleRequest();
        });
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
	
	// THEN THE SLIDE SHOW EDIT CONTROLS
	editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
	    editController.processAddSlideRequest();
	});
        removeSlideButton.setOnAction(e ->{
            editController.processRemoveRequest(selectedSlide);
        });
        moveUpButton.setOnAction(e ->{
            editController.processMoveUpRequest(selectedSlide);
        });
        moveDownButton.setOnAction(e -> {
            editController.processMoveDownRequest(selectedSlide);
        });
        
        // to select a slide
        slidesEditorPane.setOnMouseClicked(e ->{
           
            if(selectedSlide == null){
               for(Slide slide: slideShow.getSlides()){
                if(slide.isSelected()){
                    selectedSlide = slide;
                    slideShow.setSelectedSlide(slide);
                    System.out.println(slide.getCaption());
                }
            } 
            }
            else{
                selectedSlide.setSelected(false);
                for(Slide slide: slideShow.getSlides()){
                if(slide.isSelected() && !selectedSlide.equals(slide)){
                    selectedSlide = slide;
                    selectedSlide.setSelected(true);
                    slideShow.setSelectedSlide(slide);
                    System.out.println(slide.getCaption());
                }
            } 
            }           
            
            reloadSlideShowPane(slideShow);
            
        });
        
        
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW,	TOOLTIP_NEW_SLIDE_SHOW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW,	TOOLTIP_LOAD_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW,	TOOLTIP_SAVE_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_SLIDE_SHOW,	TOOLTIP_VIEW_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        editSlideShowTitleButton = initChildButton(fileToolbarPane,ICON_EDIT_SLIDE_SHOW_TITLE, TOOLTIP_EDIT_SLIDE_SHOW_TITLE,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        titleLabel = new Label(slideShow.getTitle());
        titleLabel.setAlignment(Pos.CENTER);
        fileToolbarPane.getChildren().add(titleLabel);
    }

    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());
        //ADD APPLICATION ICON
        primaryStage.getIcons().add(new Image("file:" + PATH_ICONS + ICON_WINDOW));
        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
	ssmPane.setTop(fileToolbarPane);	
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveSlideShowButton.setDisable(saved);
	viewSlideShowButton.setDisable(false);
	
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
            
        if(!slideShow.hasSlides())
            viewSlideShowButton.setDisable(false);
        else
            viewSlideShowButton.setDisable(true);
        //UPDATE TITLE TEXT
        titleLabel.setText("\t\t" + slideShow.getTitle());
        if(slideShow.getTitle() == null)
            titleLabel.setText("");
        titleLabel.setStyle("-fx-font-size: 24; -fx-font-weight: bold");
        
        
    }

    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {

        slidesEditorPane.getChildren().clear();
	for (Slide slide : slideShowToLoad.getSlides()) {
	    SlideEditView slideEditor = new SlideEditView(slide);
            slideEditor.setStyle("-fx-border-color: transparent; -fx-border-width: 0px");
            if(slideShowToLoad.getSelectedSlide().equals(slide)){
                slideEditor.setStyle("-fx-border-color: red; -fx-border-width: 3px");
            }
	    slidesEditorPane.getChildren().add(slideEditor);
            
	}
        //enable/disable respective buttons
        if(selectedSlide != null){
            removeSlideButton.setDisable(false);
            moveUpButton.setDisable(false);
            moveDownButton.setDisable(false);
            if(slideShow.getSlides().indexOf(selectedSlide) == 0)
                moveUpButton.setDisable(true);
            if(slideShow.getSlides().indexOf(selectedSlide) == slideShow.getSlides().size() - 1)
                moveDownButton.setDisable(true);
        }
    }

    private void viewSlideShow() {
        if(!slideShow.hasSlides())
            return;
        viewIndex = 0;
        viewSlide(slideShow.getSlides().get(viewIndex));
        
    }
    
    private void viewSlide(Slide slide){
        try {
        Stage stage = new Stage();
        BorderPane pane = new BorderPane();
        pane.setMinSize(700, 700);
        ObservableList<Slide> slides = slideShow.getSlides();
        
        Button prev = new Button("<---");
        prev.setDisable(true);
        Button next = new Button("--->");
        next.setDisable(true);
        pane.setLeft(prev);
        pane.setRight(next);
        Label titl = new Label(slide.getCaption());
        titl.setAlignment(Pos.CENTER);
        pane.setTop(new Label(slideShow.getTitle()));
        prev.setOnAction( e -> {
            stage.close();
            viewSlide(slides.get(--viewIndex));
        });
        next.setOnAction( e ->{
            stage.close();
            viewSlide(slides.get(++viewIndex));
        });
              
        if(viewIndex != 0)
            prev.setDisable(false);
        if(viewIndex != slides.size()-1)
            next.setDisable(false);
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
            String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
            File file = new File(imagePath);
            URL fileURL = file.toURI().toURL();
            Image slideImage = new Image(fileURL.toExternalForm());
            ImageView view = new ImageView(slideImage);
            double scaledWidth = 450;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    view.setFitWidth(scaledWidth);
	    view.setFitHeight(scaledHeight);
            pane.setCenter(view);
            Label capt = new Label(slide.getCaption());
            capt.setAlignment(Pos.CENTER);
            pane.setBottom(capt);
            
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(SlideShowMakerView.class.getName()).log(Level.SEVERE, null, ex);
        }
  
    }
}