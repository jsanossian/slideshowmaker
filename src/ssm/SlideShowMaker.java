package ssm;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import xml_utilities.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.ICON_WINDOW;
import static ssm.StartupConstants.PATH_DATA;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;

/**
 * SlideShowMaker is a program for making custom image slideshows. It will allow
 * the user to name their slideshow, select images to use, select captions for
 * the images, and the order of appearance for slides.
 *
 * @author McKilla Gorilla & _____________
 */
public class SlideShowMaker extends Application {
    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    SlideShowFileManager fileManager = new SlideShowFileManager();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    SlideShowMakerView ui = new SlideShowMakerView(fileManager);

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Prompt user for language
        promptLanguage();
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(TITLE_WINDOW);

	    // NOW START THE UI IN EVENT HANDLING MODE
	    ui.startUI(primaryStage, appTitle);
	} // THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
	else {
	    // LET THE ERROR HANDLER PROVIDE THE RESPONSE
	    ErrorHandler errorHandler = ui.getErrorHandler();
	    errorHandler.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "ERROR", UI_PROPERTIES_FILE_NAME);
	    System.exit(0);
	}
    }
    
    private static void promptLanguage(){
        //Create combo box to select language
        ComboBox lang = new ComboBox(FXCollections.observableArrayList(
                "English",
                "Español"
          ));
        //Default language = English
        lang.setValue("English");
        Button ok = new Button("OK");
        //Create Stage with title
        Stage langStage = new Stage();
        langStage.setTitle("Select language/Seleccionar un idioma");
        langStage.getIcons().add(new Image("file:" + PATH_ICONS + ICON_WINDOW));
        langStage.setMinHeight(150);
        langStage.setMinWidth(400);
         // if user presses close button, exit the program
        langStage.setOnCloseRequest(e -> {
            System.exit(0);
        });
        //Create FlowPane to automatically sort button and combo boxes
        FlowPane langPane = new FlowPane(10, 10, lang);
        langPane.getChildren().add(ok);
        //Add components To new Scene on the Stage
        langStage.setScene(new Scene(langPane, 100, 100));
        ok.setOnMouseClicked(e -> {
            if(lang.getValue().toString().equals("Español"))
                UI_PROPERTIES_FILE_NAME = "properties_ES.xml";
            langStage.close();
        });
        //show the stage
        langStage.showAndWait();
    }
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
	    props.loadProperties(UI_PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, "ERROR", PATH_DATA);
            return false;
        }        
    }

    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
	launch(args);
    }
}
