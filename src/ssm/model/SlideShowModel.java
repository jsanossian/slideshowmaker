package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.SlideShowMakerView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & _____________
 */
public class SlideShowModel {
    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    //the amount of slides in the slide show initialized to 0
    int size = 0;
    
    
    public SlideShowModel(SlideShowMakerView initUI) {
	ui = initUI;
	slides = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
    }
    
    public ObservableList<Slide> getSlides() {
	return slides;
    }
    
    public Slide getSelectedSlide() {
	return selectedSlide;
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
	
        selectedSlide = initSelectedSlide;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
	selectedSlide = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(   String initImageFileName,
			    String initImagePath) {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath);
	slides.add(slideToAdd);
	
        selectedSlide = slideToAdd;
	ui.reloadSlideShowPane(this);
        size++;
    }
        /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     * @param initCaption caption for the slide to add
     */
    public void addSlide(   String initImageFileName,
			    String initImagePath,
                            String initCaption) {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath);
        slideToAdd.setCaption(initCaption);
	slides.add(slideToAdd);
	
        selectedSlide = slideToAdd;
	ui.reloadSlideShowPane(this);
        size++;
    }
    
    public void removeSlide(Slide slide){
        slides.remove(slide);
        ui.reloadSlideShowPane(this);
    }
    public boolean hasSlides(){
        if(size == 0) return false;
        else return true;
        
    }
}